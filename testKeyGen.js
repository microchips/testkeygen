const fetch = require('node-fetch');

const sharedKey = 'ymB58JExX7hOKzbF72TBuN5EUF7X6PGAZFhOTGCLMns',
      apiKey = 'w9p4Our8ffIqUqxEVblUgKY7UTiwc5NUZ2OoggM_IVg',
      productCode = "PR",
      baseURL = 'https://api.licensespring.com/api/v4/webhook';

function GenerateHeaders(){
    const signingDate = (new Date()).toUTCString(),
          signingString = `licenseSpring\ndate: ${signingDate}`;

    const signature = crypto.createHmac('sha256', sharedKey).update(signingString).digest('base64');

    return {
            'Content-Type': 'application/json',
            'Date': signingDate,
            'Authorization' : `algorithm="hmac-sha256",headers="date",signature="${signature}",apikey="${apiKey}"`,
        };
}

const getLicenses = async (productQuantity) => {
    const headers = GenerateHeaders()

    const response = await fetch(baseURL + '/license' + `?product=${productCode}&quantity=${productQuantity}`, {
        method: 'GET',
        headers: headers
    })
    console.log("/license response", await response.json());
}

getLicenses(10);